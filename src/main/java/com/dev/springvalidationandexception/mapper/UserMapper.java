package com.dev.springvalidationandexception.mapper;

import com.dev.springvalidationandexception.dto.UserRequest;
import com.dev.springvalidationandexception.dto.UserResponse;
import com.dev.springvalidationandexception.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public abstract class UserMapper {
    @Mapping(target = "id", ignore = true)
    public abstract User toUserEntity(UserRequest userRequest);

    public abstract UserResponse toUserResponse(User user);

    public List<UserResponse> toListUserResponse(List<User> users) {
        if (null == users) {
            return null;
        }

        return users.stream().map(this::toUserResponse).toList();
    }
}
