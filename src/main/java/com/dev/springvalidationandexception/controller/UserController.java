package com.dev.springvalidationandexception.controller;

import com.dev.springvalidationandexception.dto.UserRequest;
import com.dev.springvalidationandexception.dto.UserResponse;
import com.dev.springvalidationandexception.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<UserResponse>> findAllUsers() {
        return ResponseEntity.ok(this.userService.findAllUser());
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> findUserById(@PathVariable Integer id) {
        return ResponseEntity.ok(this.userService.findUserById(id));
    }

    @PostMapping
    public ResponseEntity<UserResponse> insertUser(@RequestBody @Valid UserRequest userRequest) {
        return ResponseEntity.ok(this.userService.insertUser(userRequest));
    }
}
