package com.dev.springvalidationandexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringValidationAndExceptionApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringValidationAndExceptionApplication.class, args);
    }

}
