package com.dev.springvalidationandexception.service;

import com.dev.springvalidationandexception.dto.UserRequest;
import com.dev.springvalidationandexception.dto.UserResponse;
import com.dev.springvalidationandexception.entity.User;
import com.dev.springvalidationandexception.mapper.UserMapper;
import com.dev.springvalidationandexception.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UserResponse findUserById(Integer id) {
        User foundUser = this.userRepository.findById(id).orElse(null);
        return this.userMapper.toUserResponse(foundUser);
    }

    public List<UserResponse> findAllUser() {
        List<User> users = this.userRepository.findAll();
        return this.userMapper.toListUserResponse(users);
    }

    public UserResponse insertUser(UserRequest userRequest) {
        User newUser = this.userMapper.toUserEntity(userRequest);
        User userSaved = this.userRepository.save(newUser);
        return this.userMapper.toUserResponse(userSaved);
    }
}
