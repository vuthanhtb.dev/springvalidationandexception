package com.dev.springvalidationandexception.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserResponse {
    private Integer id;
    private String name;
    private String address;
    private String email;
    private int age;
    private double salary;
}
