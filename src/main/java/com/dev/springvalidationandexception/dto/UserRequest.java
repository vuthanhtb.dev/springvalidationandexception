package com.dev.springvalidationandexception.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor(staticName = "build")
@NoArgsConstructor
@Data
public class UserRequest {
    @NotNull(message = "username should not be null")
    private String name;

    @NotNull(message = "address should not be null")
    private String address;

    @Email(message = "this is not a correct email")
    private String email;

    @Min(value = 18, message = "age is less than 18")
    @Max(value = 60, message = "age is more than 60")
    private int age;

    @Min(value = 0, message = "salary is less than 0")
    private double salary;
}
